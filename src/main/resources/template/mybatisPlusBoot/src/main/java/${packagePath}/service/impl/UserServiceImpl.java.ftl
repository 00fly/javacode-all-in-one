package ${packageName}.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${packageName}.entity.User;
import ${packageName}.mapper.UserMapper;
import ${packageName}.service.IUserService;

/**
 *
 * User 表数据服务层接口实现类
 *
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService
{
    
    @Override
    public boolean deleteAll()
    {
        return retBool(baseMapper.deleteAll());
    }
    
    @Override
    public List<User> selectListBySQL()
    {
        return baseMapper.selectListBySQL();
    }
    
    @Override
    public List<User> selectListByWrapper(Wrapper wrapper)
    {
        return baseMapper.selectListByWrapper(wrapper);
    }
}