package ${package.Controller};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
#if(${superControllerClassPackage})
import ${superControllerClassPackage};
#end

import com.fly.common.result.JsonResult;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};

/**
 * <p>
 * ${table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Controller
@RequestMapping("#if(${package.ModuleName})/${package.ModuleName}#end/${table.entityPath}")
#if(${superControllerClass})
public class ${table.controllerName} extends ${superControllerClass} 
{
#else
public class ${table.controllerName} 
{
#end
    @Autowired
    private ${table.serviceName} ${table.entityPath}service;  
    
    
    @RequestMapping({"/", ""})
    public String index(ModelMap model)
    {
        model.addAttribute("data", ${table.entityPath}service.selectList(null));
        return "${table.entityPath}/index";
    }


    @RequestMapping("/preSave")
    public String preSave(ModelMap model, @RequestParam(value = "id", required = false)
    #foreach($field in ${table.fields})#if(${field.keyFlag})${field.propertyType}#end#end id)
    {
        if (id != null)
        {
            model.addAttribute("data", ${table.entityPath}service.selectById(id));
        }
        return "${table.entityPath}/save";
    }
    
    @ResponseBody
    @RequestMapping("/save")
    public JsonResult save(${entity} entity)
    {
        if (entity.getId() == null)
        {
            return ${table.entityPath}service.insert(entity) ? renderSuccess("添加成功") : renderError("添加失败");
        }
        else
        {
            return ${table.entityPath}service.updateById(entity) ? renderSuccess("修改成功") : renderError("修改失败");
        }
    }
    
    @ResponseBody
    @RequestMapping("/delete")
    public JsonResult delete(@RequestParam(value = "id", required = false)
    #foreach($field in ${table.fields})#if(${field.keyFlag})${field.propertyType}#end#end id)
    {
        return ${table.entityPath}service.deleteById(id) ? renderSuccess("删除成功") : renderError("删除失败");
    }
	
}
