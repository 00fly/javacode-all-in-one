package com;

import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.context.WebApplicationContext;

/**
 * 
 * SpringBootApplication 启动类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SpringBootApplication
public class SpringJpaApplication implements CommandLineRunner
{
    @Value("${r'${server.port}'}")
    Integer port;
    
    @Autowired
    WebApplicationContext applicationContext;
    
    public static void main(String[] args)
    {
        SpringApplication.run(SpringJpaApplication.class, args);
    }
    
    @Override
    @CacheEvict(value = "users", allEntries = true)
    public void run(String... args)
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            Runtime.getRuntime().exec("cmd /c start /min http://127.0.0.1:" + port + "/" + applicationContext.getServletContext().getContextPath() + "/doc.html");
        }
    }
}
