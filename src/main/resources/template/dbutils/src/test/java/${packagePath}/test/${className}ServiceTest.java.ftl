package ${packageName}.test;

import org.junit.Test;

import ${packageName}.service.${className}Service;
import ${packageName}.service.impl.${className}ServiceImpl;

/**
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ${className}ServiceTest
{
    ${className}Service ${instanceName}Service = ${className}ServiceImpl.getInstance();
    
    @Test
    public void test()
    {
        ${instanceName}Service.queryAll();
    }
}
