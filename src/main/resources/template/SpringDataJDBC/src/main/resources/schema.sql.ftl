SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` SERIAL PRIMARY KEY, 
  `name` varchar(255) DEFAULT NULL,
  `age` smallint(6) DEFAULT NULL
);

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '00fly', '20'); 
INSERT INTO `user` VALUES ('2', '00fly', '21'); 
INSERT INTO `user` VALUES ('3', '00fly', '22'); 
INSERT INTO `user` VALUES ('4', '00fly', '23'); 
INSERT INTO `user` VALUES ('5', '00fly', '24'); 
INSERT INTO `user` VALUES ('6', '00fly', '25'); 
INSERT INTO `user` VALUES ('7', '00fly', '26'); 
INSERT INTO `user` VALUES ('8', '00fly', '27'); 
INSERT INTO `user` VALUES ('9', '00fly', '28');

