package ${packageName};

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * SpringBootApplication 启动类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@EnableJdbcRepositories
@SpringBootApplication(scanBasePackages = {"com.fly", "${packageName}"})
public class JdbcApplication implements CommandLineRunner
{
    @Value("$\{server.port}")
    Integer port;
    
    @Autowired
    ServletContext servletContext;
    
    public static void main(String[] args)
    {
        SpringApplication.run(JdbcApplication.class, args);
    }
    
    @Override
    public void run(String... args)
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            log.info("★★★★★★★★  now open Browser ★★★★★★★★ ");
            String ip = InetAddress.getLocalHost().getHostAddress();
            String url = "http://" + ip + ":" + port + servletContext.getContextPath();
            Runtime.getRuntime().exec("cmd /c start /min " + url + "/${instanceName}/");
            Runtime.getRuntime().exec("cmd /c start /min " + url + "/doc.html");
            // Runtime.getRuntime().exec("cmd /c start /min " + url + "/h2-console");
        }
    }
}
