package com.fly.demo.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 登录情况
 * 
 * @author jack
 */
@ApiModel(value = "登录情况")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KcLoginDetail
{
    @ApiModelProperty(value = "PK,uuid")
    private String id;
    
    @ApiModelProperty(value = "学生唯一标识")
    private String idno;
    
    @ApiModelProperty(value = "FK，课程类型编号")
    private String courseTypeId;
    
    @ApiModelProperty(value = "FK，课程编号")
    private String courseId;
    
    @ApiModelProperty(value = "登录时间（2021-06-08 14:08:50）")
    private String loginTime;
    
    @ApiModelProperty(value = "离开时间（2021-06-08 14:28:50）")
    private String leaveTime;
    
    @ApiModelProperty(value = "时长")
    private Integer hours;
    
    @ApiModelProperty(value = "创建人")
    private String createBy;
    
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    
    @ApiModelProperty(value = "更新人")
    private String updateBy;
    
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    
    @ApiModelProperty(value = "删除")
    private Boolean delete;
}