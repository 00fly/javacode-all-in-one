package ${packageName}.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.entity.JsonResult;
import ${packageName}.domain.${className};
import ${packageName}.service.${className}Service;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * JdbcController
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Api(tags = "用户操作接口")
@RestController
@RequestMapping(value = "/rest/${instanceName}")
public class Rest${className}Controller
{
    @Autowired
    private ${className}Service ${instanceName}Service;
    
    @ApiOperationSupport(order = 10)
    @ApiOperation("01 add用户")
    @PostMapping("/add")
    public JsonResult<?> add()
    {
        ${instanceName}Service.add();
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 20)
    @ApiOperation("02 delete用户")
    @ApiImplicitParam(name = "id", value = "用户id", required = true)
    @PostMapping("/delete/{id}")
    public JsonResult<?> delete(@PathVariable Long id)
    {
        ${instanceName}Service.delete(id);
        return JsonResult.success();
    }
    
    @ApiOperationSupport(order = 30)
    @ApiOperation("03 分页查询用户1")
    @ApiImplicitParam(name = "pageNo", value = "页号", required = true)
    @GetMapping("/queryPage/{pageNo}")
    public Page<${className}> queryPage(@PathVariable Integer pageNo)
    {
        pageNo = Math.max(1, pageNo);
        Pageable pageable = PageRequest.of(pageNo - 1, 5);
        return ${instanceName}Service.findAll(pageable);
    }
    
    @ApiOperationSupport(order = 40)
    @ApiOperation("04 分页查询用户2")
    @ApiImplicitParam(name = "pageNo", value = "页号", required = true)
    @GetMapping("/queryPageList/{pageNo}")
    public List<${className}> queryPageList(@PathVariable Integer pageNo)
    {
        pageNo = Math.max(1, pageNo);
        Pageable pageable = PageRequest.of(pageNo - 1, 5);
        return ${instanceName}Service.findBy(pageable);
    }
    
    @ApiOperationSupport(order = 50)
    @ApiOperation("05 查询全部用户")
    @GetMapping("/query")
    public List<${className}> queryAll()
    {
        return ${instanceName}Service.queryAll();
    }
    
}
