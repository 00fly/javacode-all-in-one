package com.fly.core;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

/**
 * 
 * Service Query支撑类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Component
public class ServiceSupport
{
    @PersistenceContext
    private EntityManager em;
    
    public NativeQuery nativeQuery()
    {
        return new NativeQuery(em);
    }
    
    public JPQLQuery jpqlQuery()
    {
        return new JPQLQuery(em);
    }
}
