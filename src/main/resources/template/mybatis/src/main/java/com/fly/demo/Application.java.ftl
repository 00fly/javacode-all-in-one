package com.fly.demo;

import org.apache.commons.lang3.SystemUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.context.WebApplicationContext;

/**
 * application
 *
 * @author jack
 */
@SpringBootApplication
@MapperScan("com.**.mapper")
@ComponentScan({"com.fly.core","com.fly.demo"})
public class Application implements CommandLineRunner
{
    @Value("$\{server.port}")
    Integer port;
    
    @Autowired
    WebApplicationContext applicationContext;
    
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }
    
    @Override
    public void run(String... args)
        throws Exception
    {
        if (SystemUtils.IS_OS_WINDOWS && port > 0)
        {
            String url = "http://127.0.0.1:" + port + applicationContext.getServletContext().getContextPath();
            Runtime.getRuntime().exec("cmd /c start /min " + url + "/doc.html");
        }
    }
}
