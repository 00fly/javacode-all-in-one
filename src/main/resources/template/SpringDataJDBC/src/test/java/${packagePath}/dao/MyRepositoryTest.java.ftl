package ${packageName}.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.test.context.junit4.SpringRunner;

import ${packageName}.domain.${className};

import lombok.extern.slf4j.Slf4j;

interface MyRepository extends PagingAndSortingRepository<${className}, Long>
{
    /**
     * age>?
     */
    public List<${className}> findByAgeGreaterThan(Integer age);
    
    /**
     * 分页
     * 
     * @param name
     * @param pageable
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<${className}> findByName(String name, Pageable pageable);
    
    /**
     * name=? order by age desc
     */
    public List<${className}> findByNameOrderByAgeDesc(String name);
}

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class MyRepositoryTest
{
    @Autowired
    MyRepository myRepository;
    
    @Test
    public void test1()
    {
        log.info("## findByNameOrderByAgeDesc(\"00fly\"):");
        myRepository.findByNameOrderByAgeDesc("00fly").forEach(u -> log.info("{}", u));
    }
    
    @Test
    public void test2()
    {
        Pageable pageable = PageRequest.of(0, 5);
        List<${className}> data = myRepository.findByName("00fly", pageable);
        log.info("## findByName(\"00fly\",pageable): {}", data);
    }
    
    @Test
    public void test3()
    {
        log.info("## findByAgeGreaterThan(25):");
        myRepository.findByAgeGreaterThan(25).forEach(u -> log.info("{}", u));
    }
}
