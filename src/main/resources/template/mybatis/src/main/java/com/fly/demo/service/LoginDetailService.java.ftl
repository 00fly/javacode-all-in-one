package com.fly.demo.service;

import com.fly.core.entity.JsonResult;

/**
 * 登录记录
 *
 * @author jack
 */
public interface LoginDetailService
{
    
    JsonResult<?> loginRecord(String idNo, String courseTypId, String courseId);
    
    JsonResult<?> logoutRecord(String idNo, String courseTypId, String courseId);
}
