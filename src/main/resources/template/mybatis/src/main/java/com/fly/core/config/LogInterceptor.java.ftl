package com.fly.core.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import lombok.extern.slf4j.Slf4j;

/**
 * 日志拦截器
 * 
 * @author jack
 */
@Component
@Slf4j
public class LogInterceptor implements HandlerInterceptor
{
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
        throws Exception
    {
        log.info("REQ>>> {}:{} {} {} {}", request.getRemoteAddr(), request.getRemotePort(), request.getMethod(), request.getRequestURI(), request.getQueryString());
        return true;
    }
    
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
        throws Exception
    {
        log.info("REQ<<< {}:{} {} {}", request.getRemoteAddr(), request.getRemotePort(), response.getStatus(), response.getContentType());
    }
    
}
