package com.fly.demo.web;

import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fly.core.entity.JsonResult;
import com.fly.demo.service.LoginDetailService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * 登录登出记录
 *
 * @author jack
 */
@CrossOrigin
@RestController
@Api(tags = "登录登出记录接口")
public class LoginDetailController
{
    
    @Autowired
    private LoginDetailService loginDetailService;
    
    @ApiOperation("登录记录接口")
    @PostMapping("/login-record")
    public JsonResult<?> loginRecord(@Validated @RequestBody LoginRecordArgs args)
    {
        return loginDetailService.loginRecord(args.getIdNo(), args.getCourseTypeId(), args.getCourseId());
    }
    
    @ApiOperation("退出登录记录接口")
    @PostMapping("/logout-record")
    public JsonResult<?> logoutRecord(@Validated @RequestBody LoginRecordArgs args)
    {
        return loginDetailService.logoutRecord(args.getIdNo(), args.getCourseTypeId(), args.getCourseId());
    }
    
    @Data
    @ApiModel
    private static class LoginRecordArgs
    {
        @ApiModelProperty(value = "学生唯一标识", required = true)
        @NotEmpty
        private String idNo;
        
        @ApiModelProperty(value = "FK，课程类型编号", required = true)
        @NotEmpty
        private String courseTypeId;
        
        @ApiModelProperty(value = "FK，课程编号", required = true)
        @NotEmpty
        private String courseId;
    }
}
