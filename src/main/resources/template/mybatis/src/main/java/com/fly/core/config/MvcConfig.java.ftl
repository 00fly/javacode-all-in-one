package com.fly.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mvc config
 * 
 * @author jack
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer
{
    
    @Autowired
    private LogInterceptor logInterceptor;
    
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(logInterceptor).addPathPatterns("/**").excludePathPatterns("/swagger-ui/**", "/v2/api-docs", "/swagger-resources", "/swagger-resources/**", "/doc.html", "/webjars/**");
        
    }
}
