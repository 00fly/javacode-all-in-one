package ${packageName}.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class ${className}RepositoryTest
{
    @Autowired
    ${className}Repository ${instanceName}Repository;
    
    @Test
    public void test()
    {
        long count = ${instanceName}Repository.count();
        log.info("#### ${instanceName}Repository.count():{}", count);
        Assert.isTrue(count > 0, "count error");
    }
}
