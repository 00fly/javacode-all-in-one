logging:
   level:
      org:
         hibernate:
            SQL: debug
server:
   servlet: 
     context-path: /
   port: 8080
spring:
   datasource:
      driver-class-name: ${driver}
      type: com.zaxxer.hikari.HikariDataSource
      url: ${dburl}?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&useSSL=false&zeroDateTimeBehavior=convertToNull
      username: ${username}
      password: ${password}
      hikari:
         auto-commit: true
         connection-test-query: SELECT 1
         connection-timeout: 30000
         idle-timeout: 30000
         max-lifetime: 1800000
         maximum-pool-size: 15
         minimum-idle: 5
         pool-name: DatebookHikariCP
   jpa:
      open-in-view: true
      hibernate:
        naming:
          physical-strategy: org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
   mvc:
      view:
         prefix: /WEB-INF/views/
         suffix: .jsp