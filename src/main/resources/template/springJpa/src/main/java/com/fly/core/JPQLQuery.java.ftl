package com.fly.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.internal.QueryImpl;
import org.hibernate.transform.Transformers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * 
 * JPQL 执行类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class JPQLQuery implements DataRepository
{
    private EntityManager em;
    
    private String jpql;
    
    private String countJpql;
    
    private Object[] params = {};
    
    private Map<String, Object> paramMap = new HashMap<>();
    
    public JPQLQuery(EntityManager em)
    {
        super();
        this.em = em;
    }
    
    public JPQLQuery setJpql(String jpql)
    {
        this.jpql = jpql;
        return this;
    }
    
    /**
     * 设置记录总条数jpql
     * 
     * @param countJpql
     * @return
     * @see [类、类#方法、类#成员]
     */
    public JPQLQuery setCountJpql(String countJpql)
    {
        this.countJpql = countJpql;
        return this;
    }
    
    public JPQLQuery setParams(Object[] params)
    {
        this.params = params;
        return this;
    }
    
    public JPQLQuery setParams(Map<String, Object> params)
    {
        this.paramMap = params;
        return this;
    }
    
    @Override
    public Map<String, Object> map()
    {
        Query query = em.createQuery(jpql).setFirstResult(0).setMaxResults(1);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<Map<String, Object>> list = query.getResultList();
        if (list.isEmpty())
        {
            return Collections.emptyMap();
        }
        return list.get(0);
    }
    
    @Override
    public List<Map<String, Object>> maps()
    {
        Query query = em.createQuery(jpql);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }
    
    @Override
    public List<Map<String, Object>> maps(int length)
    {
        Query query = em.createQuery(jpql).setFirstResult(0).setMaxResults(length);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }
    
    @Override
    public List<Map<String, Object>> maps(int pageNo, int pageSize)
    {
        Query query = em.createQuery(jpql).setFirstResult((pageNo - 1) * pageSize).setMaxResults(pageSize);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        return query.getResultList();
    }
    
    @Override
    public Page mapPage(int pageNo, int pageSize)
    {
        Pageable pageable = new PageRequest(pageNo - 1, pageSize);
        return new PageImpl<>(maps(pageNo, pageSize), pageable, count());
    }
    
    @Override
    public <T> T model(Class<T> resultClass)
    {
        Query query = em.createQuery(jpql).setFirstResult(0).setMaxResults(1);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.aliasToBean(resultClass));
        Object object = query.getSingleResult();
        if (object != null)
        {
            return (T)object;
        }
        return null;
    }
    
    @Override
    public <T> List<T> models(Class<T> resultClass)
    {
        Query query = em.createQuery(jpql);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.aliasToBean(resultClass));
        return query.getResultList();
    }
    
    @Override
    public <T> List<T> models(Class<T> resultClass, int length)
    {
        Query query = em.createQuery(jpql).setFirstResult(0).setMaxResults(length);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.aliasToBean(resultClass));
        return query.getResultList();
    }
    
    @Override
    public <T> List<T> models(Class<T> resultClass, int pageNo, int pageSize)
    {
        Query query = em.createQuery(jpql).setFirstResult((pageNo - 1) * pageSize).setMaxResults(pageSize);
        setParams(query);
        query.unwrap(QueryImpl.class).setResultTransformer(Transformers.aliasToBean(resultClass));
        return query.getResultList();
    }
    
    @Override
    public <T> Page<T> modelPage(Class<T> resultClass, int pageNo, int pageSize)
    {
        Pageable pageable = new PageRequest(pageNo - 1, pageSize);
        return new PageImpl(models(resultClass, pageNo, pageSize), pageable, count());
    }
    
    /**
     * 设置参数
     * 
     * @param query
     * @see [类、类#方法、类#成员]
     */
    private void setParams(Query query)
    {
        if (jpql.contains("?"))
        {
            int i = 1;
            for (Object obj : params)
            {
                query.setParameter(i, obj);
                i++;
            }
        }
        else if (!paramMap.isEmpty())
        {
            for (String key : paramMap.keySet())
            {
                query.setParameter(key, paramMap.get(key));
            }
        }
    }
    
    @Override
    public long count()
    {
        if (countJpql != null)
        {
            Query countQuery = em.createQuery(countJpql);
            setParams(countQuery);
            Object obj = countQuery.getSingleResult();
            return Long.parseLong(obj.toString());
        }
        return maps().size();
    }
    
    @Override
    public boolean exists()
    {
        if (countJpql != null)
        {
            Query countQuery = em.createQuery(countJpql);
            setParams(countQuery);
            Object obj = countQuery.getSingleResult();
            return Long.parseLong(obj.toString()) > 0;
        }
        return map() != null;
    }
    
    @Override
    public long updateOrDelete()
    {
        Query query = em.createQuery(jpql);
        setParams(query);
        return query.executeUpdate();
    }
    
}
