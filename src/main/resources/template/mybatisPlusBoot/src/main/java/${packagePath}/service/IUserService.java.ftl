package ${packageName}.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import ${packageName}.entity.User;

/**
 *
 * User 表数据服务层接口
 *
 */
public interface IUserService extends IService<User>
{
    
    boolean deleteAll();
    
    public List<User> selectListBySQL();
    
    public List<User> selectListByWrapper(Wrapper wrapper);
}