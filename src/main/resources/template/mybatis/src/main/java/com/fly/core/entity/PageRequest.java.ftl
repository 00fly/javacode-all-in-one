package com.fly.core.entity;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModel;

/**
 * 分页请求
 */
@ApiModel
public class PageRequest
{
    /**
     * 当前页码
     */
    @Min(value = 1, message = "页码不能小于1")
    private int pageNum;
    
    /**
     * 每页数量
     */
    @Range(min = 5, max=200, message = "每页数量必须在{min}-{max}")
    private int pageSize;
    
    public int getPageNum()
    {
        return pageNum;
    }
    
    public void setPageNum(int pageNum)
    {
        this.pageNum = pageNum;
    }
    
    public int getPageSize()
    {
        return pageSize;
    }
    
    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }
}