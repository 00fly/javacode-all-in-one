<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${packageName}.mapper.UserMapper">

    <!-- 通用查询结果列 -->
    <sql id="Base_Column_List">
        test_id AS testId, name, age, test_type AS testType, role, phone
    </sql>

    <delete id="deleteAll">
        DELETE FROM USER
    </delete>

    <select id="selectListByWrapper" resultType="${packageName}.entity.User">
        SELECT * FROM USER
        <!-- 判断 wrapper 是否为空 emptyOfWhere -->
        <where>
            ${r'${ew.sqlSegment}'}
        </where>
    </select>
</mapper>