 
 
package ${pknEntity};

<#if hasDate == true>
import java.util.Date;
</#if>
<#if hasBigDecimal == true>
import java.math.BigDecimal;
</#if>

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
<#if hasDate == true>
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
</#if>
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.ketayao.ketacustom.entity.Idable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description = "数据对象")
@Entity
@Table(name="${tableName}")
public class ${className} implements Idable<Long>
{
    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "${pk.name}")
    private Long id;
    
    <#list columns as column>
    /**
     * ${column.comment}
     */    
    @ApiModelProperty(value = "${column.comment}"<#if column.nullable == false>, required = true</#if>)
    <#if column.javaType == "String">@NotBlank(message = "${column.fieldName}不能为空")</#if>
    @Column(name="${column.name}"<#if column.nullable == false>, nullable=false</#if>, length=${column.size})
    <#if column.type == "DATE">
    @Temporal(TemporalType.DATE)
    <#elseif column.type == "DATETIME">
    @Temporal(TemporalType.TIMESTAMP)
    <#elseif column.type == "TIME">
    @Temporal(TemporalType.TIME)
    <#elseif column.type == "TIMESTAMP">
    @Temporal(TemporalType.TIMESTAMP)
    </#if>
    private ${column.javaType} ${column.fieldName};
    
    </#list>
    /**
     * @return the id
     */
    public Long getId() 
    {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id) 
    {
        this.id = id;
    }
    <#list columns as column>
    
    /**
     * @param ${column.fieldName} the ${column.fieldName} to set
     */
    public void ${column.setMethod}(${column.javaType} ${column.fieldName})
    {
       this.${column.fieldName} = ${column.fieldName};
    }
    
    /**
     * @return the ${column.fieldName} 
     */
    public ${column.javaType} ${column.getMethod}()
    {
       return this.${column.fieldName};
    }
    </#list>
}
