<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop" 
    xmlns:context="http://www.springframework.org/schema/context" 
    xmlns:jdbc="http://www.springframework.org/schema/jdbc" 
    xmlns:tx="http://www.springframework.org/schema/tx"
    xmlns:jpa="http://www.springframework.org/schema/data/jpa"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans 
        http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
        http://www.springframework.org/schema/aop 
        http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context-4.0.xsd
        http://www.springframework.org/schema/jdbc 
        http://www.springframework.org/schema/jdbc/spring-jdbc-4.0.xsd
        http://www.springframework.org/schema/tx 
        http://www.springframework.org/schema/tx/spring-tx-4.0.xsd
        http://www.springframework.org/schema/data/jpa 
        http://www.springframework.org/schema/data/jpa/spring-jpa-1.3.xsd"
        default-lazy-init="false">

    <description>Spring公共配置 </description>
    
    <!-- 资源文件 -->
    <bean id="propertyPlaceholderConfigurer" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="location" value="classpath:jdbc.properties" />
    </bean>    
    
    <!-- 数据源 -->
    <bean id="dataSource" class="org.apache.tomcat.jdbc.pool.DataSource" destroy-method="close">
        <property name="poolProperties">
                <bean class="org.apache.tomcat.jdbc.pool.PoolProperties">
                <property name="driverClassName" value="${r'${jdbc.driver}'}" />
                <property name="url" value="${r'${jdbc.url}'}" />
                <property name="username" value="${r'${jdbc.username}'}" />
                <property name="password" value="${r'${jdbc.password}'}" />
                <property name="jmxEnabled" value="true" />
                <property name="testWhileIdle" value="false" />
                <property name="testOnBorrow" value="true" />
                <property name="validationInterval" value="30000" />
                <property name="testOnReturn" value="false" />
                <property name="validationQuery" value="SELECT 1 from dual" />
                <property name="timeBetweenEvictionRunsMillis" value="30000" />
                <property name="maxIdle" value="2" />
                <property name="maxActive" value="10" />
                <property name="initialSize" value="2" />
                <property name="maxWait" value="10000" />
                <property name="removeAbandonedTimeout" value="60000" />
                <property name="minEvictableIdleTimeMillis" value="30000" />
                <property name="minIdle" value="1" />
                <property name="logAbandoned" value="true" />
                <property name="removeAbandoned" value="true" />
                <property name="jdbcInterceptors" value="org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer" />
            </bean>
        </property>
    </bean>
    

    <!-- 使用annotation 自动注册bean, 并保证@Required、@Autowired的属性被注入 -->
    <context:component-scan base-package="com.ketayao,com.fly.core,${packageName}">
        <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
        <context:exclude-filter type="annotation" expression="org.springframework.web.bind.annotation.ControllerAdvice"/>
    </context:component-scan>
    
    <!-- Spring Data Jpa配置 , 扫描base-package下所有继承于Repository<T,ID>的接口-->
     <jpa:repositories base-package="com.ketayao,${packageName}"  transaction-manager-ref="transactionManager" entity-manager-factory-ref="entityManagerFactory"/>

    <!-- Jpa Entity Manager 配置 -->
    <bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean" depends-on="dataSource"> 
        <property name="dataSource" ref="dataSource"/>
        <property name="jpaVendorAdapter" ref="hibernateJpaVendorAdapter"/>
        <property name="packagesToScan" value="com.ketayao,${packageName}"/>
        <property name="jpaProperties">
            <props>
                <prop key="hibernate.current_session_context_class">thread</prop>
                
                <!-- 抓取策略 -->
                <prop key="hibernate.max_fetch_depth">1</prop>
                <prop key="hibernate.default_batch_fetch_size">4</prop>
                <prop key="hibernate.jdbc.fetch_size">30</prop>
                <prop key="hibernate.jdbc.batch_size">50</prop>
                
                <!-- 缓存 -->
                <prop key="hibernate.cache.use_second_level_cache">false</prop>
                <prop key="hibernate.cache.use_query_cache">false</prop>

                <!-- 用于调试的属性 -->
                <prop key="hibernate.hbm2ddl.auto">none</prop>
                <prop key="hibernate.show_sql">true</prop>
                <prop key="hibernate.format_sql">true</prop> 
                <prop key="hibernate.generate_statistics">true</prop>
                <prop key="hibernate.use_sql_comments">true</prop>
                <!-- end 用于调试的属性 -->            
            </props>
        </property>
    </bean>

    <bean id="hibernateJpaVendorAdapter" class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
        <property name="databasePlatform" value="${r"${hibernate.dialect}"}"/>
    </bean>

    <!-- 事务管理器配置, Jpa单数据源事务 -->
    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory"/>
    </bean>

    <!-- 使用annotation定义事务 -->
    <tx:annotation-driven transaction-manager="transactionManager"/>
    
    <bean id="validator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean"/>
    
</beans>