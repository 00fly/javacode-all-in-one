package ${packageName}.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ${packageName}.domain.${className};
import ${packageName}.service.${className}Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * ${className}Controller
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Controller
@RequestMapping(value = "/${instanceName}")
public class ${className}Controller
{
    @Autowired
    ${className}Service ${instanceName}Service;
    
    /**
     * String 日期转换为 Date
     * 
     * @param dataBinder
     * @see [类、类#方法、类#成员]
     */
    @InitBinder
    public void dataBinder(WebDataBinder dataBinder)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(df, true));
    }
    
    /**
     * 新增/更新数据
     * 
     * @param ${instanceName}
     * @return
     * @see [类、类#方法、类#成员]
     */
    @PostMapping("/add")
    public String add(@Valid
    @ModelAttribute("item") ${className} ${instanceName}, Errors errors, Model model)
    {
        if (errors.hasErrors())
        {
            StringBuilder errorMsg = new StringBuilder();
            for (ObjectError error : errors.getAllErrors())
            {
                errorMsg.append(error.getDefaultMessage()).append(" ");
            }
            if (errorMsg.length() > 0)
            {
                log.info("errors message={}", errorMsg);
            }
            model.addAttribute("page", ${instanceName}Service.findAll(PageRequest.of(0, 10)));
            return "/${instanceName}/show";
        }
        ${instanceName}Service.save(${instanceName});
        return "redirect:/${instanceName}/list";
    }
    
    /**
     * 删除数据
     * 
     * @param id
     * @return
     * @see [类、类#方法、类#成员]
     */
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id)
    {
        ${instanceName}Service.delete(id);
        return "redirect:/${instanceName}/list";
    }
    
    /**
     * 列表展示
     * 
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @GetMapping({"/", "/list"})
    public String list(@RequestParam(defaultValue = "1") int pageNo, Model model)
    {
        pageNo = Math.max(pageNo, 1);
        ${className} ${instanceName} = new ${className}();
        if (RandomUtils.nextInt(1, 10) > 1)
        {
            // ${instanceName}.setName(RandomStringUtils.randomNumeric(5));
            // ${instanceName}.setAge(RandomUtils.nextInt(10, 60));
        }
        model.addAttribute("item", ${instanceName});
        model.addAttribute("page", ${instanceName}Service.findAll(PageRequest.of(pageNo - 1, 10)));
        return "/${instanceName}/show";
    }
    
    /**
     * 编辑数据
     * 
     * @param id
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id, Model model)
    {
        model.addAttribute("item", ${instanceName}Service.findById(id));
        model.addAttribute("page", ${instanceName}Service.findAll(PageRequest.of(0, 10)));
        return "/${instanceName}/show";
    }
}
