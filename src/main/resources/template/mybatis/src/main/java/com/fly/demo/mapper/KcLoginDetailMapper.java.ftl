package com.fly.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fly.demo.model.KcLoginDetail;

/**
 * 登录情况
 * 
 * @author jack
 */
//@Mapper
public interface KcLoginDetailMapper
{
    int deleteByPrimaryKey(String id);
    
    int insert(KcLoginDetail record);
    
    int insertSelective(KcLoginDetail record);
    
    KcLoginDetail selectByPrimaryKey(String id);
    
    int updateByPrimaryKeySelective(KcLoginDetail record);
    
    int updateByPrimaryKey(KcLoginDetail record);
    
    KcLoginDetail selectByCondition(@Param("idNo") String idNo, @Param("courseTypeId") String courseTypeId, @Param("courseId") String courseId);
}