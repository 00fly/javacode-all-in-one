package com.fly.test;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;

import ${pknDAO}.impl.${className}DAOImpl;
import ${pknServiceImpl}.${className}ServiceImpl;

/**
 * 
 * Main
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class Main
{
    /**
     * Main
     * 
     * @param args
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
    {
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
        new Thread(new MyRun()).start();
    }
}

/**
 * 
 * 单元测试线程
 * 
 * @author 00fly
 * @version [版本号, 2018年9月23日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
class MyRun implements Runnable
{
    public MyRun()
    {
        super();
    }
    
    @Override
    public void run()
    {
        try
        {
            ${className}ServiceImpl.getInstance().queryAll();
            TimeUnit.SECONDS.sleep(RandomUtils.nextLong(1, 20));
            
            ${className}DAOImpl.getInstance().queryAll();
            TimeUnit.SECONDS.sleep(RandomUtils.nextLong(1, 20));
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
