<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context" 
    xmlns:util="http://www.springframework.org/schema/util"
    xsi:schemaLocation="
    http://www.springframework.org/schema/beans 
    http://www.springframework.org/schema/beans/spring-beans-4.3.xsd 
    http://www.springframework.org/schema/context 
    http://www.springframework.org/schema/context/spring-context-4.3.xsd 
    http://www.springframework.org/schema/util 
    http://www.springframework.org/schema/util/spring-util-4.3.xsd">

    <!-- 引入属性文件 -->
    <context:property-placeholder location="classpath:druid-pool.properties" />

    <!-- Service包(自动注入) -->
    <context:component-scan base-package="${packName}" use-default-filters="false">
        <context:include-filter type="annotation" expression="org.springframework.stereotype.Service" />
    </context:component-scan>

    <import resource="classpath:spring/spring-mybatis.xml" />
</beans>