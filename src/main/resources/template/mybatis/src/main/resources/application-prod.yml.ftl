server:
  port: 8080

knife4j:
  enable: true

spring:
  datasource:
    url: jdbc:mysql://localhost:3306/kcrecord?zeroDateTimeBehavior=convertToNull&serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8
    username: root
    password: root
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource
    druid:
      test-while-idle: true
      test-on-borrow: true
      validation-query: select 1

logging:
  level:
    root: info
    com.ronghui.kcrecord: debug
