package com.fly;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import com.fly.demo.Application;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ClientTest
{
    @Autowired
    private WebApplicationContext wac;
    
    @BeforeEach
    public void setup()
    {
        int i = 1;
        for (String beanName : wac.getBeanDefinitionNames())
        {
            log.info("{}.\t{}", i, beanName);
            i++;
        }
    }
    
    @Test
    public void test()
    {
        System.out.println("hello world");
    }
}
