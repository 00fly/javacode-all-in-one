<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN">
	<Properties>
		<Property name="baseDir">logs</Property>
		<Property name="fname">kcrecord</Property>
	</Properties>
	<Appenders>
		<Console name="Console" target="SYSTEM_OUT">
			<PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %c{1.} - %msg%n" />
		</Console>
		<RollingRandomAccessFile name="rollingFile" fileName="$\{baseDir}/$\{fname}.log" filePattern="$\{baseDir}/$\{fname}.%d{MM-dd}.log.gz">
			<PatternLayout>
				<Pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5p] [%c{1}] [%-10t] - %m%n</Pattern>
			</PatternLayout>
			<Policies>
				<TimeBasedTriggeringPolicy interval="1" />
			</Policies>
			<DefaultRolloverStrategy>
				<Delete basePath="$\{baseDir}" maxDepth="2">
					<IfFileName glob="$\{fname}*.log.gz" />
					<IfLastModified age="30d" />
				</Delete>
			</DefaultRolloverStrategy>
		</RollingRandomAccessFile>
		<RollingRandomAccessFile name="errFile" fileName="$\{baseDir}/err_$\{fname}.log" filePattern="$\{baseDir}/err_$\{fname}.%d{MM-dd}.log.gz">
			<PatternLayout>
				<Pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5p] [%c{1}] [%-10t] - %m%n</Pattern>
			</PatternLayout>
			<Policies>
				<TimeBasedTriggeringPolicy />
			</Policies>
			<DefaultRolloverStrategy />
		</RollingRandomAccessFile>
	</Appenders>
	<Loggers>
		<Logger name="errlog" level="warn">
			<AppenderRef ref="errFile" />
		</Logger>
		<AsyncRoot level="debug">
			<AppenderRef ref="rollingFile" />
			<AppenderRef ref="errFile" level="warn" />
			<AppenderRef ref="Console" />
		</AsyncRoot>
		<!-- <AsyncLogger name="com.foo.Bar" level="trace" > <AppenderRef ref="RandomAccessFile"/> </AsyncLogger> -->
	</Loggers>
</Configuration>
