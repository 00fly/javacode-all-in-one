#JavaCode-All-In-One
# 一、打包

```bash
mvn clean package
```


# 二、运行


![运行界面](https://images.gitee.com/uploads/images/2018/0830/085149_dd841ac6_722815.png "运行界面")

- [springmvc_ibatis_plus](https://gitee.com/00fly/springmvc_ibatis_plus)
- [springmvc_dbutils_plus](https://gitee.com/00fly/springmvc_dbutils_plus)
- springmvc_mybatis_plus
- [MyBatis Generator](http://www.mybatis.org/generator/)
- SpringJpa
- SpringBoot dbutils
- SpringBoot JPA
- SpringBoot MybatisPlus

大整合！

欢迎点击链接加入技术讨论群【Java 爱码少年】：https://jq.qq.com/?_wv=1027&k=4AuWuZu