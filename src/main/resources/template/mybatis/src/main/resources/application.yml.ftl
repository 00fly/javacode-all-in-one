server:
  port: 8080

knife4j:
  enable: true

spring:
   profiles: 
      active: 
        - dev

mybatis:
  mapper-locations: classpath*:db/*.xml


logging:
  level:
    root: info
    com.ronghui.kcrecord: debug