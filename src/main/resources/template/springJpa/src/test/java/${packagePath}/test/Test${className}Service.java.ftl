package ${packageName}.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import ${pknService}.${className}Service;

/**
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号, 2017-09-27]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@RunWith(SpringRunner.class)
@ContextConfiguration({"/applicationContext.xml"})
@Transactional
public class Test${className}Service
{
    @Autowired
    ${className}Service ${instanceName}Service;
    
    @Test
    public void test()
    {
        ${instanceName}Service.get(1L);
    }
}
