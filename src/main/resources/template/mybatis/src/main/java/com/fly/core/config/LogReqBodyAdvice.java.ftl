package com.fly.core.config;

import java.lang.reflect.Type;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import lombok.extern.slf4j.Slf4j;

/**
 * 请求body日志
 * 
 * @author jack
 */
@Slf4j
@ControllerAdvice
public class LogReqBodyAdvice extends RequestBodyAdviceAdapter
{
    
    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass)
    {
        return true;
    }
    
    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType)
    {
        log.info("BODY>>> {}", body);
        return super.afterBodyRead(body, inputMessage, parameter, targetType, converterType);
    }
    
}
