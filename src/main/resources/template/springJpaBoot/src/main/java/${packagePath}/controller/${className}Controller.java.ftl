
package ${pknController};

import java.util.List;
import java.util.Map;
<#if hasDate == true>
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
</#if>

import javax.servlet.ServletRequest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
<#if hasDate == true>
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
</#if>
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ketayao.ketacustom.util.dwz.AjaxObject;
import com.ketayao.ketacustom.util.dwz.Page;
import com.ketayao.ketacustom.util.persistence.DynamicSpecifications;
import ${pknEntity}.${className};
import ${pknService}.${className}Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "数据接口")
@Controller
@RequestMapping("/${requestMapping}")
public class ${className}Controller 
{

    @Autowired
    private ${className}Service ${instanceName}Service;
    
    private static final String CREATE = "${requestMapping}/create";
    
    private static final String UPDATE = "${requestMapping}/update";
    
    private static final String LIST = "${requestMapping}/list";
    
    private static final String VIEW = "${requestMapping}/view";
    
    <#if hasDate == true>
    @InitBinder
    public void dataBinder(WebDataBinder dataBinder) 
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        dataBinder.registerCustomEditor(Date.class,    new CustomDateEditor(df, true));
    }
    
    </#if>
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String preCreate(Map<String, Object> map) 
    {
        return CREATE;
    }
    
    @ApiOperation("新增数据")
    @RequestMapping(value="/create", method=RequestMethod.POST)
    public @ResponseBody String create(@Valid ${className} ${instanceName}) 
    {
        ${instanceName}Service.saveOrUpdate(${instanceName});
        return AjaxObject.newOk("${functionName}添加成功！").toString();
    }
    
    @ModelAttribute("preload${className}")
    public ${className} preload(@RequestParam(value = "id", required = false) Long id) 
    {
        if (id != null) 
        {
            ${className} ${instanceName} = ${instanceName}Service.get(id);
            return ${instanceName};
        }
        return null;
    }
    
    @RequestMapping(value="/update/{id}", method=RequestMethod.GET)
    public String preUpdate(@PathVariable Long id, Map<String, Object> map) 
    {
        ${className} ${instanceName} = ${instanceName}Service.get(id);
        map.put("${instanceName}", ${instanceName});
        return UPDATE;
    }
    
    @RequestMapping(value="/update", method=RequestMethod.POST)
    public @ResponseBody String update(@Valid @ModelAttribute("preload${className}")${className} ${instanceName}) 
    {
        ${instanceName}Service.saveOrUpdate(${instanceName});
        return AjaxObject.newOk("${functionName}修改成功！").toString();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public @ResponseBody String delete(@PathVariable Long id) 
    {
        ${instanceName}Service.delete(id);
        return AjaxObject.newOk("${functionName}删除成功！").setCallbackType("").toString();
    }
    
    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public @ResponseBody String deleteMany(Long[] ids) 
    {
        for (int i = 0; i < ids.length; i++) 
        {
            ${className} ${instanceName} = ${instanceName}Service.get(ids[i]);
            ${instanceName}Service.delete(${instanceName}.getId());
        }
        return AjaxObject.newOk("${functionName}删除成功！").setCallbackType("").toString();
    }

    @RequestMapping(value={"/","/list"}, method={RequestMethod.GET, RequestMethod.POST})
    public String list(ServletRequest request, Page page, Map<String, Object> map) 
    {
        Specification<${className}> specification = DynamicSpecifications.bySearchFilter(request, ${className}.class);
        List<${className}> ${instanceName}s = ${instanceName}Service.findByExample(specification, page);
        map.put("page", page);
        map.put("${instanceName}s", ${instanceName}s);
        return LIST;
    }
    
    @RequestMapping(value="/view/{id}", method={RequestMethod.GET})
    public String view(@PathVariable Long id, Map<String, Object> map) 
    {
        ${className} ${instanceName} = ${instanceName}Service.get(id);
        map.put("${instanceName}", ${instanceName});
        return VIEW;
    }
}
