
package ${packageName}.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;

@Data
@Table("${tableName}")
public class ${className}
{
    @Id
    @Column("${pk.name}")
    private Long id;
    
  
    <#list columns as column>
    /**
     * ${column.comment}
     */    
    <#if column.javaType == "String">@NotBlank(message = "${column.fieldName}不能为空")</#if>
    @Column("${column.name}")
    <#if column.type == "DATE">
    @Temporal(TemporalType.DATE)
    <#elseif column.type == "DATETIME">
    @Temporal(TemporalType.TIMESTAMP)
    <#elseif column.type == "TIME">
    @Temporal(TemporalType.TIME)
    <#elseif column.type == "TIMESTAMP">
    @Temporal(TemporalType.TIMESTAMP)
    </#if>
    private ${column.javaType} ${column.fieldName};
    
    </#list>
}
