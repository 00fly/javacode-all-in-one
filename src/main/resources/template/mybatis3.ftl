<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN" 
         "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
<generatorConfiguration>
    <#if driver='com.mysql.jdbc.Driver'><classPathEntry location="lib/mysql-connector.jar"/></#if>
    <#if driver='oracle.jdbc.driver.OracleDriver'><classPathEntry location="lib/ojdbc14.jar"/></#if>
    <context id="DB2Tables" targetRuntime="MyBatis3" defaultModelType="flat">
        <property name="autoDelimitKeywords" value="true"/><!--关键字是否添加分隔符-->
        <property name="beginningDelimiter" value="`"/>
        <property name="endingDelimiter" value="`"/>
        <commentGenerator>
            <property name="suppressDate" value="true"/><!-- 是否生成注释代时间戳-->
            <property name="suppressAllComments" value="true"/><!-- 是否取消注释 -->
        </commentGenerator>
        <jdbcConnection driverClass="${driver}" connectionURL="${dburl}" userId="${username}" password="${password}" />
        <javaTypeResolver>
            <property name="forceBigDecimals" value="false"/>
        </javaTypeResolver>
        <!--生成Model类存放位置-->
        <javaModelGenerator targetPackage="${packName}.model" targetProject="${projectSrcDir}">
            <property name="enableSubPackages" value="false"/>
            <property name="trimStrings" value="true"/>
        </javaModelGenerator>
        <!--生成映射文件存放位置--> 
        <sqlMapGenerator targetPackage="db" targetProject="${projectResourceDir}">
            <property name="enableSubPackages" value="false"/>
        </sqlMapGenerator>
        <!--生成Dao类存放位置-->
        <javaClientGenerator type="XMLMAPPER" targetPackage="${packName}.mapper" targetProject="${projectSrcDir}" >
            <property name="enableSubPackages" value="false"/>
        </javaClientGenerator>
        <#list tables as table>
        <table tableName="${table}" domainObjectName="${domains[table]}" enableInsert="true" enableSelectByPrimaryKey="true" 
            enableSelectByExample="false" enableDeleteByExample="false" enableCountByExample="false" enableUpdateByExample="false" >
          <property name="useActualColumnNames" value="false"/>
          <generatedKey column="id" sqlStatement="JDBC"/>
        </table>
        </#list>
    </context>
</generatorConfiguration>
