package ${packageName}.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ${packageName}.dao.${className}Repository;
import ${packageName}.domain.${className};
import ${packageName}.service.${className}Service;

@Service
public class ${className}ServiceImpl implements ${className}Service
{
    @Autowired
    private ${className}Repository ${instanceName}Repository;
    
    @Override
    public void add()
    {
        List<${className}> list = new ArrayList<>();
        for (int i = 0; i < 10; i++)
        {
            ${className} ${instanceName} = new ${className}();
            // ${instanceName}.setName(RandomStringUtils.randomAlphabetic(10));
            // ${instanceName}.setAge(RandomUtils.nextInt(10, 40));
            list.add(${instanceName});
        }
        ${instanceName}Repository.saveAll(list);
    }
    
    @Override
    public void delete(Long id)
    {
        ${instanceName}Repository.deleteById(id);
    }
    
    @Override
    public void save(${className} ${instanceName})
    {
        ${instanceName}Repository.save(${instanceName});
    }
    
    @Override
    public ${className} findById(Long id)
    {
        return ${instanceName}Repository.findById(id).get();
    }
    
    @Override
    public List<${className}> queryAll()
    {
        return ${instanceName}Repository.queryAll();
    }
    
    @Override
    public Page<${className}> findAll(Pageable pageable)
    {
        return ${instanceName}Repository.findAll(pageable);
    }
    
    @Override
    public List<${className}> findBy(Pageable pageable)
    {
        return ${instanceName}Repository.findBy(pageable);
    }
}
