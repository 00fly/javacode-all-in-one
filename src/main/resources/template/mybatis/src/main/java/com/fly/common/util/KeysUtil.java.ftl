package com.fly.common.util;

import java.util.UUID;

/**
 * 简单的key生成工具
 *
 * @author jack
 */
public class KeysUtil
{
    
    public static String uuid()
    {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
