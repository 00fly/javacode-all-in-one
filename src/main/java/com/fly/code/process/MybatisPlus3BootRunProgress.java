package com.fly.code.process;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.keta.generate.core.GenerateFactory;
import com.keta.generate.util.Resources;
import com.keta.generate.util.StringUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * MybatisPlus3BootRunProgress
 * 
 * @author 00fly
 * @version [版本号, 2017-7-25]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
public class MybatisPlus3BootRunProgress implements IRunnableWithProgress
{
    private String driver;
    
    private String dburl;
    
    private String username;
    
    private String password;
    
    private String packName;
    
    private String projectDir;
    
    private String[] tabName;
    
    String prefix;
    
    String codeType;
    
    public MybatisPlus3BootRunProgress(String driver, String dburl, String username, String password, String packName, String projectDir, String[] tabName, String prefix, String codeType)
    {
        super();
        this.driver = driver;
        this.dburl = dburl;
        this.username = username;
        this.password = password;
        this.packName = packName;
        this.projectDir = projectDir;
        this.tabName = tabName;
        new File(projectDir + "src\\").mkdirs();
        this.prefix = prefix;
        this.codeType = codeType;
    }
    
    @Override
    public void run(IProgressMonitor monitor)
        throws InvocationTargetException, InterruptedException
    {
        // 在当前目录，创建并运行脚本
        try
        {
            monitor.beginTask("生成代码", IProgressMonitor.UNKNOWN);
            monitor.subTask("自动生成DAO、Map、MODEL 代码中......");
            creatAndRunMyBatisPlus(driver, dburl, username, password, packName, projectDir, tabName);
            monitor.done();
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            throw new InvocationTargetException(e.getCause(), e.getMessage());
        }
    }
    
    // 运行 MyBatis 代码创建程序
    private void creatAndRunMyBatisPlus(String driver, String dburl, String username, String password, String packName, String projectSrcDir, String[] tabName)
        throws Exception
    {
        // 生成模板下的文件
        for (String tableName : tabName)
        {
            String className;
            if (StringUtils.isEmpty(prefix))
            {
                className = StringUtil.camelCase(tableName, true);
            }
            else
            {
                className = StringUtil.camelCase(tableName.replaceAll("(?i)^" + prefix, ""), true);
            }
            Resources.init(driver, dburl, username, password, tableName, packName, projectDir, className, codeType);
            GenerateFactory factory = new GenerateFactory();
            factory.genJavaTemplate();
        }
    }
}
