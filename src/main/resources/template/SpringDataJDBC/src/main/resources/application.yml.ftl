server:
  port: 8080
  servlet:
    context-path: /
    session:
      timeout: 1800
spring:
  mvc:
    view:
      prefix: /WEB-INF/views/
      suffix: .jsp
  profiles:
    active:
    - dev
    
knife4j:
  enable: true