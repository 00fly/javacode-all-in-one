package ${packageName}.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.SpringJpaApplication;
import ${pknService}.${className}Service;

/**
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号,  ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringJpaApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Test${className}Service
{
    @Autowired
    ${className}Service ${instanceName}Service;
    
    @Test
    public void test()
    {
        ${instanceName}Service.get(1L);
    }
}
