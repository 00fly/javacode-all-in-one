package com.fly.core;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

/**
 * 
 * 数据访问接口
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SuppressWarnings("rawtypes")
public interface DataRepository
{
    /**
     * 查询单条记录
     * 
     * @return Map&lt;String, Object&gt;
     */
    public Map<String, Object> map();
    
    /**
     * 查询全部记录
     * 
     * @return List &lt;Map&lt;String, Object&gt;&gt;
     */
    public List<Map<String, Object>> maps();
    
    /**
     * 查询前length条记录
     * 
     * @param length
     * @return List &lt;Map&lt;String, Object&gt;&gt;
     */
    public List<Map<String, Object>> maps(int length);
    
    /**
     * 分页查询记录
     * 
     * @param pageNo 页号,起始值1
     * @param pageSize 页条数
     * @return List &lt;Map&lt;String, Object&gt;&gt;
     */
    public List<Map<String, Object>> maps(int pageNo, int pageSize);
    
    /**
     * 分页查询记录
     * 
     * @param pageNo 页号,起始值1
     * @param pageSize 页条数
     * @return Page
     * @see [类、类#方法、类#成员]
     */
    public Page mapPage(int pageNo, int pageSize);
    
    /**
     * 查询单条记录
     * 
     * @return T
     */
    public <T> T model(Class<T> resultClass);
    
    /**
     * 查询全部记录
     * 
     * @return List &lt;T&gt;
     */
    public <T> List<T> models(Class<T> resultClass);
    
    /**
     * 查询前length条记录
     * 
     * @param length
     * @return List &lt;T&gt;
     */
    public <T> List<T> models(Class<T> resultClass, int length);
    
    /**
     * 分页查询记录
     * 
     * @param pageNo 页号,起始值1
     * @param pageSize 页条数
     * @return List &lt;T&gt;
     */
    public <T> List<T> models(Class<T> resultClass, int pageNo, int pageSize);
    
    /**
     * 分页查询记录
     * 
     * @param pageNo 页号,起始值1
     * @param pageSize 页条数
     * @return Page &lt;T&gt;
     * @see [类、类#方法、类#成员]
     */
    public <T> Page<T> modelPage(Class<T> resultClass, int pageNo, int pageSize);
    
    /**
     * 查询记录条数
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    public long count();
    
    /**
     * 是否存在数据
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    public boolean exists();
    
    /**
     * 更新/删除记录
     * 
     * @return 更新/删除记录条数
     * @see [类、类#方法、类#成员]
     */
    public long updateOrDelete();
    
}
