package ${packageName}.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import ${packageName}.domain.${className};

public interface ${className}Repository extends PagingAndSortingRepository<${className}, Long>
{
    @Query("SELECT * FROM ${tableName}")
    public List<${className}> queryAll();
    
    // public List<${className}> findByNameLike(String name);
    
    public List<${className}> findBy(Pageable pageable);
}
