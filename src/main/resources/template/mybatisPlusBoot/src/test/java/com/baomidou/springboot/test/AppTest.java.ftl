package com.baomidou.springboot.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fly.Application;

/**
 * Created by jobob on 17/5/16.
 */
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class AppTest
{
    
    @Test
    public void test()
    {
        int count = 1;
        System.out.println(count);
        Assert.assertEquals(1, count);
    }
}
