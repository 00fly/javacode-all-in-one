package ${packageName}.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import ${packageName}.JdbcApplication;
import ${packageName}.dao.${className}Repository;

import lombok.extern.slf4j.Slf4j;

/**
 * Junit 单元测试
 * 
 * @author 00fly
 * @version [版本号, 2018-09-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Slf4j
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JdbcApplication.class)
public class ${className}ServiceTest
{
    @Autowired
    ${className}Repository ${instanceName}Repository;
    
    @Autowired
    ApplicationContext applicationContext;
    
    @Before
    public void before()
    {
        log.info("★★★★★★★★ ApplicationContext = {}", applicationContext);
        int i = 1;
        for (String beanName : applicationContext.getBeanDefinitionNames())
        {
            log.info("{}.\t{}", i, beanName);
            i++;
        }
    }
    
    @Test
    public void test()
    {
        StringBuilder message = new StringBuilder();
        ${instanceName}Repository.findAll().forEach(category -> {
            message.append(category.toString());
        });
        log.info("{}", message);
        log.info("{}", ${instanceName}Repository.queryAll());
    }
}
