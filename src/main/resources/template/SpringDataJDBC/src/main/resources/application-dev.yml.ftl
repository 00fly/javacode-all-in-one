spring:
  datasource:
    driverClassName: ${driver}
    url: ${dburl}
    username: ${username}
    password: ${password}
    type: com.alibaba.druid.pool.DruidDataSource
    sqlScriptEncoding: utf-8
    initialization-mode: always
    schema: classpath:schema.sql
  h2:
    console:
      enabled: true
      path: /h2-console
      settings:
        web-allow-others: true