package ${packageName}.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ${packageName}.domain.${className};

public interface ${className}Service
{
    void add();
    
    void delete(Long id);
    
    void save(${className} ${instanceName});
    
    ${className} findById(Long id);
    
    List<${className}> queryAll();
    
    Page<${className}> findAll(Pageable pageable);
    
    List<${className}> findBy(Pageable pageable);
}
