package com.fly.core.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.slf4j.Slf4j;

/**
 * swagger
 *
 * @author jack
 */
@Configuration
@ConditionalOnProperty(value = "knife4j.enable", havingValue = "true")
@Slf4j
public class SwaggerMvcConfig implements WebMvcConfigurer
{
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        log.info("-------------------swagger enable-----------------------");
        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
